let headers = [];
let streams = [];
let current_stream_id = 0;

function nocache() {
    return '?' + (new Date()).getTime();
}

function geturl(url) {
    return fetch(url + nocache(), { credentials: 'omit', method: 'GET', headers: headers });
}

document.addEventListener('DOMContentLoaded', function() {
    fetchStreams();
    let stream_sel = document.querySelector('#player select');
    stream_sel.addEventListener('change', (ev) => {
        playStream(stream_sel.value);
    });
    document.querySelector('#player .reload').addEventListener('click', () => {
        playStream(current_stream_id);
    });
    document.querySelector('#player .refresh').addEventListener('click', () => {
        fetchStreams();
    });
    document.querySelector('#player .to_live').addEventListener('click', () => {
        if (lathist!=null) {
            lathist.goToLive();
        }
    });
    document.querySelector('#player .stop').addEventListener('click', () => {
        closeAll();
        document.querySelector('#player').classList.remove('big');
    });
});

function fetchStreams() {
    geturl('./streams.json').then(r => r.json()).then(jobj => {
        document.querySelector('#player').style.display = 'block';
        streams = jobj;
        let selelm = document.querySelector('#player select');
        let prev = 0;
        if (selelm.value==undefined || selelm.value=='') {
            streams.forEach((stream, index) => {
                if (stream.prefer_in && navigator.userAgent.includes(stream.prefer_in)) {
                    prev = index;
                }
            });
            try {
                playStream(prev);
            } catch(e) {
                console.error("Error playing default stream", e);
            }
        } else {
            prev = selelm.value;
        }
        selelm.innerHTML = '';
        streams.forEach((stream, index) => {
            let opt = document.createElement('option');
            opt.innerText = stream.title;
            opt.value = index;
            selelm.appendChild(opt);
        });
        selelm.value = prev;
    }).catch(err => {
        console.log(err);
        alert("Failed to load streams list");
    });
}

function playStream(id) {
    current_stream_id = id;
    closeAll();
    function create(type) {
        elem = document.createElement(type);
        elem.controls = 'controls';
        document.querySelector('#player').appendChild(elem);
        return elem;
    }
    let stream = streams[id];
    let urlelem = document.querySelector('#player .url');
    urlelem.textContent = stream.url;
    urlelem.href = stream.url;
    let audio_only = stream.type == 'audio';
    let mediaelem = create(audio_only ? 'audio':'video');
    if (stream.type=='dash') {
        let player = dashjs.MediaPlayer().create();
        player.initialize(mediaelem, stream.url, true);
        player.updateSettings({
            'streaming': {
                'stableBufferTime': 4,
                'bufferTimeAtTopQualityLongForm': 4,/*
                'liveDelay': 6,
                'liveCatchUpMinDrift': 0.5,
                'liveCatchUpPlaybackRate': 0.5,
                'lowLatencyEnabled': true*/
            }
        });
    } else if (stream.type=='hls') {
        openHls(mediaelem, stream.url);
    } else if (stream.type=='ws') {
        wss = new WebSocketPlayer(mediaelem, stream);
    } else if (stream.type=='video') {
        mediaelem.src = stream.url + nocache();
    } else if (stream.type=='audio') {
        mediaelem.src = stream.url + nocache();
    }
    document.querySelector('#player').classList[audio_only ? 'remove':'add']('big');
    planned_latency = stream.type=='hls' ? 5 : stream.type=='ws' ? 1.5 : 2;
    lathist = new LatencyHistory(mediaelem);
    mediaelem.addEventListener('play', e => {
        lathist.goToLive();
    });

    tryPlay(mediaelem);
}


const unmuteOnClick = e => {
    e.target.muted = false;
    e.target.removeEventListener('click', unmuteOnClick);
    e.target.play();
    e.preventDefault();
};

function tryPlay(video) {
    let result = video.play();
    if (result!=null) {
        result.catch(() => {
            console.info("Playback inhibited by browser, please click on player");
            video.muted = true;
            video.addEventListener('click', unmuteOnClick);
            video.play();
        });
    }
}

let hls = null;
let wss = null;
let lathist = null;

function closeSources() {
    if (hls!=null) hls.destroy();
    if (wss!=null) wss.destroy();
    if (lathist!=null) lathist.destroy();
    hls = null;
    wss = null;
    lathist = null;
    document.querySelector('#player .latency').innerText = 'nic';
}
function closeAll() {
    closeSources();
    let delelem;
    while (delelem = document.querySelector('#player audio, #player video')) {
        delelem.remove();
    }
}

let planned_latency = 2;

function LatencyHistory(video) {
    this.refresh_interval = null;
    this.history = [];
    this.live_pos = null;
    this.refresh_interval = setInterval(() => {
        let bufftext = '';
        if (video.buffered==undefined || video.buffered.length==0 || video.readyState<3) {
            console.log('video not ready');
            this.reset();
            bufftext += 'loading';
        } else {
            let range_start = video.buffered.start(video.buffered.length-1);
            let range_end = video.buffered.end(video.buffered.length-1);
            let buffered = range_end - video.currentTime;
            this.history.push({time: video.currentTime, buffered: buffered});
            //bufftext += Math.round(buffered*10)/10 + 's'
            if (video.currentTime-this.history[0].time > 5) {
                // enough data collected
                let lat_min = 9999;
                let lat_max = 0;
                this.history.forEach(elem => { if (elem.buffered < lat_min) lat_min = elem.buffered; });
                this.history.forEach(elem => { if (elem.buffered > lat_max) lat_max = elem.buffered; });
                //console.log(lat_min, this.history.length);
                bufftext += '' + Math.round(lat_min*10)/10 + ' - ' + Math.round(lat_max*10)/10 + 's';
                let sync_allowed = !document.querySelector('#player .nosync').checked;
                if (sync_allowed) {
                    if (lat_min > planned_latency*4) {
                        let pos = range_end - planned_latency;
                        if (pos < range_start) pos = range_start;
                        video.currentTime = pos;
                        this.reset();
                        console.log('jump');
                    } else if (lat_min > planned_latency*1.33) {
                        video.playbackRate = 1 + (lat_min/planned_latency)*0.1;
                        console.log('pullup ' + video.playbackRate);
                        bufftext += ' >>';
                    } else if (lat_min <= planned_latency) {
                        video.playbackRate = 1;
                    }
                } else {
                    video.playbackRate = 1;
                }
            } else {
                video.playbackRate = 1;
                bufftext += '~ ' + Math.round(buffered*10)/10 + 's';
            }
            this.history = this.history.filter(elem => video.currentTime-elem.time<40);
        }
        document.querySelector('#player .latency').innerText = bufftext;
    }, 500);
    this.goToLive = () => {
        if (!(video.buffered==undefined || video.buffered.length==0 || video.readyState<3)) {
            let range_start = video.buffered.start(video.buffered.length-1);
            let range_end = video.buffered.end(video.buffered.length-1);
            let pos = range_end - planned_latency;
            if (pos < range_start) pos = range_start;
            if (video.currentTime < pos) video.currentTime = pos;
        }
    };
    this.reset = () => {
        this.history = [];
        video.playbackRate = 1;
    };
    this.destroy = () => {
        if (this.refresh_interval!=null) clearInterval(this.refresh_interval);
    };
}

function WebSocketPlayer(video, obj) {
    this.ws = new WebSocket(obj.url);
	this.codec_string = obj.codecs;
    this.ws.binaryType = 'arraybuffer';
    this.video = video;
    this.queue = [];
    this.ws.onmessage = e => {
        if (typeof e.data === 'object') {
            this.queue.push(e.data);
            if (!this.ms_initialized) {
                this.initMediaSource();
            }
            if (this.buffer) {
                this.updateBuffer();
            }
        }
    };
    this.initMediaSource = () => {
        this.ms = new MediaSource();
        this.ms.addEventListener('sourceopen', () => this.openSourceBuffer());
        this.video.src = URL.createObjectURL(this.ms);
        this.ms_initialized = true;
    };
    this.openSourceBuffer = () => {
		if (!MediaSource.isTypeSupported(this.codec_string)) {
			console.log('Codec not supported: ' + this.codec_string);
			return;
		}
		this.buffer = this.ms.addSourceBuffer(this.codec_string);
		this.buffer.mode = 'sequence';
		this.buffer.addEventListener('updateend', () => this.updateBuffer());
		this.updateBuffer();
    };
    this.removing = false;
    this.updateBuffer = () => {
        if (this.buffer.updating) return;

        if (!this.removing && video.readyState>=3 && video.currentTime>15) {
            this.removing = true;
            try {
                this.buffer.remove(0, video.currentTime-10);
            } catch(e) {
                console.log('garbage collection of buffer failed', e);
            }
            return;
        }

        if (this.queue.length > 0) {
            this.buffer.appendBuffer(this.queue.shift());
        }
        this.removing = false;
    };
    this.destroy = () => {
        this.ws.close();
        this.queue = [];
    }
    this.initMediaSource();
}

function openHls(video, videoSrc) {
  if (Hls.isSupported()) {
    hls = new Hls({
        liveSyncDurationCount: 3,
        liveMaxLatencyDurationCount: 6,
        //liveDurationInfinity: true,
        liveBackBufferLength: 4
    });
    hls.loadSource(videoSrc);
    hls.attachMedia(video);
    hls.on(Hls.Events.MANIFEST_PARSED, function() {
      tryPlay(video);
    });
  }
  // hls.js is not supported on platforms that do not have Media Source Extensions (MSE) enabled.
  // When the browser has built-in HLS support (check using `canPlayType`), we can provide an HLS manifest (i.e. .m3u8 URL) directly to the video element through the `src` property.
  // This is using the built-in support of the plain video element, without using hls.js.
  // Note: it would be more normal to wait on the 'canplay' event below however on Safari (where you are most likely to find built-in HLS support) the video.src URL must be on the user-driven
  // white-list before a 'canplay' event will be emitted; the last video event that can be reliably listened-for when the URL is not on the white-list is 'loadedmetadata'.
  else if (video.canPlayType('application/vnd.apple.mpegurl')) {
    video.src = videoSrc;
    video.addEventListener('loadedmetadata', function() {
      tryPlay(video);
    });
  }
}
